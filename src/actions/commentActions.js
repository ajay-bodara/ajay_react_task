import * as types from "./actionsTypes";
import commentsApi from "../api/commentsApi";
const uuidv1 = require('uuid/v1');

export const commentRequestInit = () => ({type: types.REQUEST_COMMENTS_INIT});
export const commentsRequestSuccess = () => ({type: types.REQUEST_COMMENTS_SUCCESS});
export const commentsRequestFailer = () => ({type: types.REQUEST_COMMENTS_FAILER});
export const commentsFetch = (payload) => ({type: types.FETCH_COMMENTS, payload});

export const fetchComments = () => {
    return function(dispatch){
        dispatch(commentRequestInit());
        return commentsApi.getComments().then(response => {
            if(response.comments){
               dispatch(commentsFetch({data: response.comments}));
               dispatch(commentsRequestSuccess());
            }else{
                dispatch(commentsRequestFailer());
            }
        }).catch(error => {
            dispatch(commentsRequestFailer());
        });
    }
}

export const postComment = (commentText) => {
    return (dispatch) => {
        return new Promise( (resolve) => {
            resolve(setTimeout(function(){
                let data = {
                    text: commentText,
                    id: uuidv1(),
                    name: 'John Doe',
                    image : 'http://placekitten.com/20/20'
                }
                dispatch({type: types.POST_COMMENTS, payload: data});
            },1500));
        });
    }
}