import * as types from "../actions/actionsTypes";

export default function (state = {}, action) {
    switch(action.type){
        case types.REQUEST_COMMENTS_INIT:
            return {
              ...state,
              isFetching: true
            }
        case types.REQUEST_COMMENTS_FAILER:
            return {
                ...state,
                isFetching: false,
                isError: true
            }
        case types.REQUEST_COMMENTS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                isError: false
            }
        case types.FETCH_COMMENTS:
            return {
                ...state,
                data: action.payload.data
            }
        case types.POST_COMMENTS:{
            state.data.splice(0, 0, action.payload)
            return {
                ...state,
            }
        }
        default:
            return {...state}
    }
}