import React, {Component} from 'react';
import {connect} from 'react-redux';

import { fetchComments, postComment } from "./actions/commentActions";
import CommentForm from './components/Form/Comment';
import CommentList from './components/Form/CommentList';
import { isNull } from 'util';
/**
 * Main Wrapper
 */
class App extends Component {

  state = {commentText: null, error: null}

  componentDidMount(){
    this.props.fetchComments();
  }

  onCommentSave = (event) => {
      event.preventDefault();
      if(isNull(this.state.commentText) || !this.state.commentText || this.state.commentText.trim().length === 0 ){
          this.setState({
              error: 'This field is required!'
          });
        return;
      }
      if(countWords(this.state.commentText.trim()) > 25){
        this.setState({
            error: 'You must enter the less than 25 words for the comment!'
        });
        return;
      }

      this.props.postComment(this.state.commentText).then(() => {
        this.setState({error: null, commentText: null});
      });
      event.target.reset();
  }

  onCommentChange = (event) => {
      this.setState({commentText: event.target.value});
  }

  render () {
    return (
      <div className="main-wrapper">
        <div className="container">
          <div className="detailBox">
            <div className="titleBox">
              <label>Comments</label>
            </div>
            <CommentForm onSubmit={this.onCommentSave} onTextChange={this.onCommentChange} error={this.state.error}/>
            <CommentList comments={this.props.comments} />
          </div>
        </div>
      </div>
    );
  }
}


function countWords(s){
	s = s.replace(/(^\s*)|(\s*$)/gi,"");
	s = s.replace(/[ ]{2,}/gi," ");
	s = s.replace(/\n /,"\n");
	return s.split(' ').length;
}

const mapStateToProps = ({comments}) => ({
    comments
});

export default connect (mapStateToProps, { fetchComments, postComment }) (App);
