import React from 'react';

import Comment from "./CommentLi";

const CommentList = ({comments}) => {

  if(comments.isFetching == undefined || comments.isFetching){
      return <div>Loading...</div>
  }
  return (
    <div className="actionBox">
      <ul className="commentList">
          {
              comments.data.map((comment) => {
                  return <Comment key={comment.id} comment={comment} />
              })
          }
      </ul>
    </div>
  );
};

export default CommentList;
