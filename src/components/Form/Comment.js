import React from 'react';

const Comment = props => {
  return (
    <div className="commentBox">
      <form className="form-horizontal" role="form" onSubmit={props.onSubmit}>
        <div className="form-group">
          <input
            className="form-control"
            type="text"
            placeholder="Add comment"
            onChange={props.onTextChange}
          />
          <span className="error-block">{props.error}</span>
        </div>
      </form>
    </div>
  );
};

export default Comment;
