import React from "react";

const CommentLi = ({comment}) => {
    return (
        <li>
          <div className="commenterImage">
            <img src={comment.image} />
          </div>
          <div className="commentText">
            <p className="">{comment.name}</p>
            <p className="">{comment.text}</p>
          </div>
          <div className="shareBox">
                 <a href="#">Like</a>{"  "}
                 <a href="#">Reply</a>{"  "}
                 <a href="#">View Replies</a>{"  "}
            </div>
        </li>
    )
}

export default CommentLi;