import {createStore, applyMiddleware, combineReducers, compose} from 'redux'
import { routerReducer, routerMiddleware } from 'react-router-redux'
import { hashHistory } from 'react-router'
import ReduxThunk from "redux-thunk";

import commentsReducers from "./reducers/commentReducers";

let initialState = {};

const Store = createStore(
    combineReducers({
        routing: routerReducer,
        comments: commentsReducers
    }),
    initialState,
    compose(
        applyMiddleware(ReduxThunk, routerMiddleware(hashHistory))
    )
);


export default Store;

