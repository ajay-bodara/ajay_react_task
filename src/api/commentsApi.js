class CommentsApi {
    static getComments() {
        return fetch(`data.json`,{
            method: 'get'
        }).then(function(response) {
            return response.json()
        }).catch(function(error){
            throw error;
        })
    }
}

export default CommentsApi;